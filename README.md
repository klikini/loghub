# ![LogHub](www/icon.png)

LogHub is a dead-simple syslog server that receives syslog messages from many networked devices
and displays them all in one web interface (but you can still choose to view messages from a subset of devices).

![Screenshot](screenshot.png)

## Running

1. Download the latest release for
   [Linux](https://gitlab.com/klikini/loghub/-/jobs/artifacts/main/raw/loghub.zip?job=Linux) or
   [Windows](https://gitlab.com/klikini/loghub/-/jobs/artifacts/main/raw/loghub.zip?job=Windows) 
1. Extract the downloaded ZIP file
1. Run `loghub` (Linux) or double-click `loghub.exe` (Windows)
1. Visit `http://host:9514` to use the web interface. It automatically displays new messages at the top of the list.

### Arguments

LogHub accepts optional arguments to control its behavior:

- `--bind` or `-b`: the interface to bind to. Defaults to `0.0.0.0` (all interfaces).
- `--syslog-port` or `-p`: the port to listen for incoming UDP packets on. Defaults to 514.
- `--web-ui-port` or `-w`: the port to host the web interface on. Defaults to 9514.
- `--message-capacity` or `-c`: the maximum number of messages to store (in memory and on disk). Defaults to 10,000.
- `--message-file` or `-f`: the name of the file to save messages to. Defaults to `messages.csv`.

Example:

```shell script
./loghub -b localhost -p 5140 --web-ui-port 9000 --message-capacity 100000 -f log.csv   
```

### Example systemd unit file

```ini
[Unit]
Description=LogHub server
After=network.target

[Service]
Type=simple
KillMode=none
User=root
WorkingDirectory=/srv/loghub
ExecStart=/srv/loghub --message-capacity 1000
KillSignal=SIGINT
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```
