import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:args/args.dart';
import 'package:loghub/log_message.dart';
import 'package:loghub/server/syslog_server.dart';
import 'package:loghub/server/web_server.dart';

ArgResults args;
bool running = true;
SendPort toWebServer;

void main(List<String> argv) async {
  var parser = ArgParser()
    ..addOption('bind', abbr: 'b', defaultsTo: InternetAddress.anyIPv4.address)
    ..addOption('syslog-port', abbr: 'p', defaultsTo: '514')
    ..addOption('web-ui-port', abbr: 'w', defaultsTo: '9514')
    ..addOption('message-capacity', abbr: 'c', defaultsTo: '10000')
    ..addOption('message-file', abbr: 'f', defaultsTo: 'messages.csv');
  args = parser.parse(argv);

  ProcessSignal.sigint.watch().listen((_) {
    running = false;
  });

  await Future.wait([
    startSyslogServer(),
    startWebServer(),
  ]);

  Isolate.current.kill();
}

Future<void> startSyslogServer() async {
  while (running) {
    var output = ReceivePort();
    await Isolate.spawn(enterSyslogServerIsolate, output.sendPort);

    await handleServerOutput(output, (m, _) {
      if (m is LogMessage) {
        toWebServer.send(m);
      }
    });
  }
}

Future<void> startWebServer() async {
  while (running) {
    var output = ReceivePort();
    await Isolate.spawn(enterWebServerIsolate, output.sendPort);

    await handleServerOutput(output, (m, _) {
      if (m is SendPort) {
        // Save a reference to the web server's SendPort for
        // sending new log messages to clients
        toWebServer = m;
      }
    });
  }
}

Future<void> handleServerOutput(
    ReceivePort port, Function(dynamic, SendPort) onMessage) async {
  SendPort sendPort;

  await for (var message in port) {
    if (message == '\0') {
      break;
    }

    if (message is SendPort) {
      // Provide program arguments to this server
      // through the SendPort it just provided
      sendPort = message;
      sendPort.send(args);
    }

    var handler = onMessage(message, sendPort);
    if (handler is Future) {
      await handler;
    }
  }
}
