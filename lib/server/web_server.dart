import 'dart:convert';
import 'dart:io';
import 'dart:isolate';

import 'package:args/args.dart';
import 'package:loghub/message_collection.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_static/shelf_static.dart';
import 'package:shelf_web_socket/shelf_web_socket.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../log_message.dart';
import 'server.dart';

WebServer webServer;

void enterWebServerIsolate(SendPort output) {
  webServer?.stop();
  webServer = WebServer(output);
}

class WebServer extends ThreadedServer {
  final List<WebSocketSink> _webSockets = [];
  shelf.Handler _staticHandler;
  shelf.Handler _webSocketHandler;
  HttpServer _httpServer;
  MessageCollector _messages;

  WebServer(SendPort output) : super(output) {
    // Serve static files
    _staticHandler = createStaticHandler('www', defaultDocument: 'index.html');

    // Upgrade ws:// connections to WebSockets
    _webSocketHandler = webSocketHandler((WebSocketChannel socket) {
      var sink = socket.sink;
      _webSockets.add(sink);
      sink.done.then((_) => _webSockets.remove(sink));

      // Send existing messages to this client
      _messages?.forEach((message) {
        var json = message.toJson();
        json['old'] = true;
        sink.add(jsonEncode(json));
      });
    });
  }

  @override
  void run(ArgResults args) async {
    // Collect and persist messages
    _messages = MessageCollector(
        int.parse(args['message-capacity']), args['message-file']);
    await _messages.readFromDisk();

    // Set up the web server
    var handler = const shelf.Pipeline()
        .addMiddleware(shelf.logRequests(
            logger: (String message, _) => print('[web] $message')))
        .addHandler((shelf.Request request) async => request.url.path == 'ws'
            ? await _webSocketHandler.call(request)
            : await _staticHandler.call(request));

    // Start the server
    var port = int.parse(Platform.environment['PORT'] ?? args['web-ui-port']);
    _httpServer = await io.serve(handler, args['bind'], port);
    print('[web] Running on ${args['bind']}:${_httpServer.port}');
  }

  @override
  void onInput(message) {
    super.onInput(message);

    if (message is LogMessage) {
      _messages.insert(message);

      var json = jsonEncode(message.toJson());
      for (var socket in _webSockets) {
        try {
          socket.add(json);
        } catch (e) {
          print('[web] Error sending message to client: $e');
        }
      }
    }
  }

  @override
  void stop() {
    try {
      _httpServer?.close();
    } catch (e) {
      print('[web] Could not close HTTP server: $e');
    }

    for (var socket in _webSockets) {
      try {
        socket.close();
      } catch (e) {
        print('[web] Could not close WebSocket: $e');
      }
    }

    _messages.writeToDisk().then((value) {
      print('[web] Stopped');
      super.stop();
    });
  }
}
