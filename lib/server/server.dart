import 'dart:io';
import 'dart:isolate';

import 'package:args/args.dart';

abstract class ThreadedServer {
  final SendPort output;
  final ReceivePort _input;

  ThreadedServer(this.output) : _input = ReceivePort() {
    // Give the creator a way to send data in
    output.send(_input.sendPort);

    // Wait for configuration
    _input.listen((message) async {
      if (message is ArgResults) {
        await run(message);
      } else {
        onInput(message);
      }
    });

    // Exit gracefully
    ProcessSignal.sigint.watch().listen((_) => stop());
  }

  /// Implementation-defined server behavior making use of passed arguments.
  void run(ArgResults args);

  void onInput(dynamic message) {}

  void stop() {
    output.send('\0');
    Isolate.current.kill(priority: Isolate.immediate);
  }
}
