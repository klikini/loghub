import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:args/args.dart';

import '../log_message.dart';
import 'server.dart';

SyslogServer syslogServer;

void enterSyslogServerIsolate(SendPort output) {
  syslogServer?.stop();
  syslogServer = SyslogServer(output);
}

class SyslogServer extends ThreadedServer {
  RawDatagramSocket _socket;
  StreamSubscription _socketListener;

  SyslogServer(SendPort output) : super(output);

  @override
  void run(ArgResults args) {
    var logPort = int.parse(args['syslog-port']);

    RawDatagramSocket.bind(args['bind'], logPort).then((socket) {
      _socket = socket;
      print('[syslog] Running on ${args['bind']}:$logPort');

      _socketListener = socket.listen((event) {
        var datagram = socket.receive();
        if (datagram == null) {
          return;
        }

        output.send(LogMessage.fromDatagram(datagram));
      });
    });
  }

  @override
  void stop() {
    try {
      _socketListener?.cancel();
    } catch (e) {
      print('[syslog] Could not cancel UDP socket listener: $e');
    }

    try {
      _socket?.close();
    } catch (e) {
      print('[syslog] Could not close UDP socket: $e');
    }

    print('[syslog] Stopped');
    super.stop();
  }
}
