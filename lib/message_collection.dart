import 'dart:async';
import 'dart:io';

import 'package:circular_buffer/circular_buffer.dart';
import 'package:csv/csv.dart';
import 'package:loghub/log_message.dart';

class MessageCollector extends CircularBuffer<LogMessage> {
  static const ListToCsvConverter _csvEncoder = ListToCsvConverter();
  static const CsvToListConverter _csvDecoder =
      CsvToListConverter(shouldParseNumbers: false);

  final File _file;

  MessageCollector(int capacity, String file, [int writeInterval = 3600])
      : _file = File(file),
        super(capacity) {
    if (!_file.existsSync()) {
      _file.createSync();
    }

    var timer = Timer.periodic(Duration(seconds: writeInterval), (_) {
      writeToDisk();
    });

    ProcessSignal.sigint.watch().listen((_) {
      timer.cancel();
    });
  }

  Future<void> readFromDisk() async {
    // Read from file
    var csv = await _file.readAsString();

    if (csv.isNotEmpty) {
      var messages = _csvDecoder.convert(csv);
      messages.forEach((m) => insert(LogMessage.fromCsv(m)));
    }
  }

  Future<void> writeToDisk() async {
    // Serialize objects
    var messages = List<List<String>>(length);
    var index = 0;
    forEach((message) => messages[index++] = message.toCsv());

    // Write to file
    await _file.writeAsString(_csvEncoder.convert(messages));
  }
}
