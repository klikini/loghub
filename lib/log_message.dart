import 'dart:io';

import 'package:uuid/uuid.dart';

class LogMessage {
  static final Uuid _uuid = Uuid();

  String id;
  DateTime received;
  InternetAddress address;
  String message;

  LogMessage._(this.id, this.received, this.address, this.message);

  LogMessage.fromDatagram(Datagram datagram) {
    id = _uuid.v1();
    received = DateTime.now();
    address = datagram.address;
    message = String.fromCharCodes(datagram.data).trim();
  }

  factory LogMessage.fromCsv(List<dynamic> csv) {
    var id = csv[0];
    var received = DateTime.parse(csv[1]);
    var address = InternetAddress(csv[2]);
    var message = csv[3];
    return LogMessage._(id, received, address, message);
  }

  List<String> toCsv() =>
      [id, received.toIso8601String(), address.address, message];

  Map<String, dynamic> toJson() => {
        'id': id,
        'received': received.toIso8601String(),
        'address': address.address,
        'message': message,
      };
}
