"use strict";

/**
 * Keep track of messages we have already received to avoid duplicates.
 * @type {Set<string>}
 */
const MessageIDs = new Set();

const Hosts = {
    /**
     * All hosts we have seen.
     * @type {Set<string>}
     */
    all: new Set(),
    /**
     * Hosts currently shown in the output.
     * @type {Set<string>}
     */
    enabled: new Set(),
}

const UI = {
    enabledCount: document.getElementById('countShowing'),
    totalCount: document.getElementById('countTotal'),
    totalPlural: document.getElementById('totalPlural'),
    status: {
        ok: document.querySelector('.status-ok'),
        connecting: document.querySelector('.status-connecting'),
    },
    output: document.getElementById('output'),
    filterEnabled: false,
    hostFilter: document.getElementById('hosts'),
};

/**
 * Add 1 to the "of x message(s)" display
 * @param {HTMLElement} display - the element containing the count
 * @returns {number} - the new value
 */
function incrementCountDisplay(display) {
    let count = parseInt(display.dataset['value'] ?? '0') + 1;
    display.dataset['value'] = count.toString();
    display.textContent = count.toLocaleString();
    return count;
}

/**
 * Update the UI to show the current connection state.
 * @param {boolean} connected - whether the websocket is currently connected
 * @returns {void}
 */
function showStatus(connected) {
    UI.status.ok.hidden = !connected;
    UI.status.connecting.hidden = connected;
}

/**
 * Add a host to the set of known hosts.
 * If the host is new, an option will be added to the filter UI.
 * @param {string} host - IP address or hostname
 * @returns {void}
 */
function addHost(host) {
    if (!Hosts.all.has(host)) {
        Hosts.all.add(host);

        let option = document.createElement('option');
        option.value = host;
        option.text = host;
        option.selected = !UI.filterEnabled;
        UI.hostFilter.append(option);
    }
}

/**
 * Enable or disable the filters.
 * @param {boolean} status - new enabled state
 */
function updateFilterStatus(status) {
    UI.filterEnabled = status;
    UI.hostFilter.disabled = !UI.filterEnabled;

    if (!UI.filterEnabled) {
        enableAllHosts();
    }
}

/**
 * Clear the host filters and make all messages visible.
 * @returns {void}
 */
function enableAllHosts() {
    for (let option of UI.hostFilter.querySelectorAll('option')) {
        option.selected = true;
    }

    UI.hostFilterAll = true;
    updateEnabledHosts();
}

/**
 * Update the UI to reflect the latest filter settings.
 * @returns {void}
 */
function updateEnabledHosts() {
    Hosts.enabled.clear();

    // Get enabled options
    for (let option of UI.hostFilter.querySelectorAll('option')) {
        if (option.selected) {
            Hosts.enabled.add(option.value);
        }
    }

    // Update table to match
    let count = 0;
    for (let row of UI.output.querySelectorAll('tr')) {
        count += (row.hidden = !Hosts.enabled.has(row.dataset['host'])) ? 0 : 1;
    }

    UI.enabledCount.dataset['value'] = count.toString();
    UI.enabledCount.textContent = count.toLocaleString();
}

/**
 * Add a message to the output table.
 * @param message
 */
function outputMessage(message) {
    let date = new Date(message['received']);
    let dateCol = document.createElement('td');
    dateCol.className = 'log-message-date';
    dateCol.innerHTML = date.toLocaleDateString() + '<br>' + date.toLocaleTimeString();

    let addressCol = document.createElement('td');
    addressCol.className = 'log-message-address';
    addressCol.textContent = message['address'];

    let messageCol = document.createElement('td');
    messageCol.className = 'log-message-content';
    messageCol.textContent = message['message'];

    let row = document.createElement('tr');
    row.id = message['id'];
    row.dataset['host'] = message['address'];
    row.append(dateCol, addressCol, messageCol);
    row.hidden = UI.filterEnabled && !Hosts.enabled.has(message['address']);
    UI.output.insertBefore(row, UI.output.firstChild);

    // Update counts
    UI.totalPlural.hidden = incrementCountDisplay(UI.totalCount) === 1;
    if (!row.hidden) {
        incrementCountDisplay(UI.enabledCount);
    }
}

/**
 * Connect to the websocket, updating the UI to show the current status as it goes.
 * @returns {void}
 */
function connect() {
    showStatus(false);
    const socket = new WebSocket('ws://' + window.location.host + '/ws');

    socket.onmessage = function (message) {
        message = JSON.parse(message.data);

        if (MessageIDs.has(message['id'])) {
            // Duplicate (happens when the server restarts and sends
            // all messages because we are a "new" client)
            return;
        }

        MessageIDs.add(message['id']);
        addHost(message['address']);
        outputMessage(message);
    }

    socket.onopen = function () {
        console.info("[WebSocket] Connected");
        showStatus(true);
    }

    socket.onclose = socket.onerror = function () {
        console.info("[WebSocket] Disconnected");
        showStatus(false);

        // Reconnect after 1 second
        setTimeout(connect, 1000);
    };
}
